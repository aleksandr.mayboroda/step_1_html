


// try to printout
let reviewsArr = [
    {
        authorName: 'ani roe',
        authorPosition: 'photographer',
        authorAvatar: 'img/what_people_say/Layer 27.png',
        reviewText: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt temporibus officiis optio
        id soluta aliquam natus ut nobis ducimus voluptate! Excepturi quaerat ullam eum esse
        architecto tempore veritatis numquam nisi.`
    },
    {
        authorName: 'hasan ali',
        authorPosition: 'Ux designer',
        authorAvatar: 'img/what_people_say/Layer 29.png',
        reviewText: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt temporibus officiis optio
        id soluta aliquam natus ut nobis ducimus voluptate! Excepturi quaerat ullam eum esse
        architecto tempore veritatis numquam nisi.`
    },
    {
        authorName: 'cris rea',
        authorPosition: 'office manager',
        authorAvatar: 'img/what_people_say/Layer 31.png',
        reviewText: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt temporibus officiis optio
        id soluta aliquam natus ut nobis ducimus voluptate! Excepturi quaerat ullam eum esse
        architecto tempore veritatis numquam nisi.`
    },
    {
        authorName: 'lora daiden',
        authorPosition: 'waiter',
        authorAvatar: 'img/what_people_say/Layer 32.png',
        reviewText: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt temporibus officiis optio
        id soluta aliquam natus ut nobis ducimus voluptate! Excepturi quaerat ullam eum esse
        architecto tempore veritatis numquam nisi.`
    },
    {
        authorName: 'mike shape',
        authorPosition: 'head',
        authorAvatar: 'img/what_people_say/Layer 34.png',
        reviewText: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt temporibus officiis optio
        id soluta aliquam natus ut nobis ducimus voluptate! Excepturi quaerat ullam eum esse
        architecto tempore veritatis numquam nisi.`
    },
    {
        authorName: 'gab shade',
        authorPosition: 'witch',
        authorAvatar: 'img/what_people_say/Layer 38.png',
        reviewText: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt temporibus officiis optio
        id soluta aliquam natus ut nobis ducimus voluptate! Excepturi quaerat ullam eum esse
        architecto tempore veritatis numquam nisi.`
    },
]

window.onload = function() {
    
    // let slider = sliderInit()
    // console.log(slider)
    // slider.apend([
    //     '<div class="swiper-slide">Slide 1"</div>',
    //     '<div class="swiper-slide">Slide 2"</div>'
    //    ]);
    // slider.append([
    //     `<h3>added slide!</h3>`
    // ])
    // slider.update()
    outputReviews(reviewsArr)
}

function outputReviews(reviewsArr,slider) {
    if(reviewsArr.length > 0)
    {
        let slideBlock = document.querySelector('.review-slider__wrapper')
        if(slideBlock)
        {
            slideBlock.insertAdjacentHTML('afterbegin',reviewsArr.map(review => {
                return `
                <div class="review-slider__slide swiper-slide">
                    <p class="review-slider__text">
                        ${review.reviewText}
                    </p>
                    <div class="review-slider__author">
                        <p class="author-name">${review.authorName}</p>
                        <p class="author-position">${review.authorPosition}</p>
                    </div>
                    <div class="review-slider__image">
                        <img class="rounded-box-shadow" src="${review.authorAvatar}" alt="${review.authorName}"
                            width="143px" height="143px">
                    </div>
                </div>
                `
            }).join(''))
             
            // slider.update()
            // slider.updateSlides()
           let slider = sliderInit()

    //            slider.appendSlide([
    //             `
    //             <div class="review-slider__slide swiper-slide">
    //             <p class="review-slider__text">
    //                 abshjfgaksjhdf askdhgjhasghl asjgbkl agkl asgjkasb klabslkhg balskhgb 
    //             </p>
    //             <div class="review-slider__author">
    //                 <p class="author-name">Alex</p>
    //                 <p class="author-position">Chief</p>
    //             </div>
    //             <div class="review-slider__image">
    //                 <img class="rounded-box-shadow" src="img/what_people_say/Layer 34.png" alt="Alex"
    //                     width="143px" height="143px">
    //             </div>
    //         </div>
    //             `,
    //    ]);
        }
    }
}

function sliderInit() {
    return new Swiper('.review-slider',{
        navigation: {
            nextEl: '.slider-button-next',
            prevEl: '.slider-button-prev',
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            dynamicBullets: true,
            renderBullet(index,className)
            {
                let slidersImages = document.querySelectorAll('.review-slider__image img')
                console.log(111,slidersImages)
                return `<span class="${className}"><img src="${slidersImages[index].src}" class="bullet-img__small" alt="className"></span>`
            }
        },
        autoHeight: true,
    })
}